import { defineStore } from 'pinia'
import { computed, ref } from "vue";
import UserService from "@/services/UserService";

export const useUserStore = defineStore(
  'UserStore',
  () => {
    const user = ref({
      email: '',
      token: ''
    })
    const error = ref('')

    const loggedIn = computed(() => {
      return !!user.value.token
    })

    function loginUser(loginData) {
      return UserService.loginUser(loginData)
        .then(response => {
          console.log(loginData.email)
          console.log(response.data.content.token)
          this.user = {
            email: loginData.email,
            token: response.data.content.token
          }
          this.error = ""
        })
        .catch(error => {
          if (error.response) {
            console.log(error.response.data);
            console.log(error.response.status);
            console.log(error.response.headers);
            this.error = error.response.data.message
          } else {
            console.log(error)
          }
        })
    }

    function logoutUser() {
      this.user = {
        email: '',
        token: ''
      }
    }

    return { user, error, loggedIn, loginUser, logoutUser }
  },
  {
    persist: {
      storage: localStorage,
    },
  }
)
