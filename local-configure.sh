#!/usr/bin/env bash
set -e
rm -Rf roles/secrets.*
ansible-galaxy install -r requirements.yml -p roles
ansible-playbook --vault-id @prompt -i 'localhost,' --connection=local local-playbook.yml
